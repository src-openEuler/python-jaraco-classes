%global _empty_manifest_terminate_build 0
Name:		python-jaraco-classes
Version:	3.4.0
Release:	1
Summary:	Utility functions for Python class constructs
License:	MIT
URL:		https://github.com/jaraco/jaraco.classes
Source0:	https://files.pythonhosted.org/packages/source/j/jaraco.classes/jaraco.classes-%{version}.tar.gz

BuildArch:	noarch

Recommends:     %{name}-help = %{version}-%{release}

BuildRequires:  python3-pip  
BuildRequires:  python3-hatchling 
BuildRequires:  python3-hatch-vcs
BuildRequires:  python3-wheel
Requires:	python3-more-itertools
Requires:	python3-sphinx
Requires:	python3-pytest
Requires:	python3-pytest-checkdocs
Requires:	python3-flake8
Requires:	python3-pytest-flake8
Requires:	python3-pytest-cov
Requires:	python3-pytest-enabler
Requires:	python3-pytest-black
Requires:	python3-pytest-mypy

%description
Utility functions for Python class constructs.

%package -n python3-jaraco-classes
Summary:	Utility functions for Python class constructs
Provides:	python-jaraco-classes
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-setuptools_scm >= 1.15
BuildRequires:	python3-sphinx
BuildRequires:	python3-pytest
%description -n python3-jaraco-classes
Utility functions for Python class constructs.

%package help
Summary:	Development documents and examples for jaraco.classes
Provides:	python3-jaraco-classes-doc
%description help
Documentation for jaraco-classes

%prep
%autosetup -n jaraco.classes-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-jaraco-classes -f filelist.lst
%dir %{python3_sitelib}/*
%{python3_sitelib}/jaraco/classes

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 23 2024 xu_ping <707078654@qq.com> - 3.4.0-1
- Update package to version 3.4.0

* Tue Aug 01 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 3.3.0-1
- Update package to version 3.3.0

* Tue Apr 25 2023 caodongxia <caodongxia@h-partners.com> - 3.2.3-4
- Adapting to the pyproject.toml compilation mode

* Mon Feb 27 2023 yaoxin <yaoxin30@h-partners.com> - 3.2.3-3
- Adaptation to setup.py and modify the patching mode to resolve installation conflicts

* Fri Feb 24 2023 yaoxin <yaoxin30@h-partners.com> - 3.2.3-2
- Fix installation conflicts between python3-jaraco-classes and python-jaraco-collections

* Sat Oct 08 2022 guozhengxin <guozhengxin@kylinos.cn> - 3.2.3-1
- Upgrade package to version 3.2.3

* Tue May 31 2022 houyingchao <houyingchao@h-partners.com> - 3.2.1-1
- Upgrade to 3.2.1

* Tue Jan 05 2021 lingsheng <lingsheng@huawei.com> - 3.1.0-2
- Add buildrequire python3-setuptools_scm

* Fri Nov 13 2020 Python_Bot <Python_Bot@openeuler.org> - 3.1.0-1
- Package init
